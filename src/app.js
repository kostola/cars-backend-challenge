'use strict'

// configs
const cfg_file   = !process.env['CONFIG_FILE'] ? "../config/development.json" : process.env['CONFIG_FILE'];
const config     = require(cfg_file);
const logger     = require('./logger')(config.log_level);
const hostname   = !config.hostname ? 'localhost' : config.hostname;
const port       = !config.port ? 3000 : config.port;

// external dependencies
const express    = require('express');
const favicon    = require('serve-favicon');
const path       = require('path');
const session    = require('express-session');
const FileStore  = require('session-file-store')(session);
const CJSON      = require('circular-json');
const moment     = require('moment');

logger.info(`Configuration file: ${cfg_file}`);
logger.info(`Log level.........: ${config.log_level}`);

//const helpers = require('./helpers')(config, logger, database);
//const logic   = require('./logic')(config, logger, database, helpers);

const app    = express();
const server = require('http').createServer(app);
const io     = require('socket.io')(server);

// configure pug template engine
app.set('view engine', 'pug');
app.set('views', path.join(__dirname, 'views'));

// Use application-level middleware for common functionality, including
// logging, parsing, and session handling
app.use(require("morgan")("combined", { "stream": logger.stream }));
//app.use(favicon(path.join(__dirname, 'public', 'images', 'favicons', 'favicon.ico')));
app.use(require('cookie-parser')());
app.use(require('body-parser').urlencoded({ extended: true }));
app.use(session({
    store: new FileStore,
    secret: config.session_secret,
    resave: true,
    saveUninitialized: true
  })
);
app.use(require('express-flash')());

// configure static files path
app.use('/modules', express.static(__dirname + '/../node_modules'));
app.use('/static', express.static(path.join(__dirname, 'public')));

// ==== Socket.io =============================================================================================

const carDataBroker = require("./broker");
carDataBroker.setInitialCars([ 'Audi A6', 'Porsche 911', 'Volvo V40' ]);

io.of("/feed").on('connection', function(socket) {
    logger.debug(`FEED: new connection from ${socket.handshake.address}`);

    socket.on('cars', () => {
        logger.debug(`FEED: ${socket.handshake.address} sent 'cars'`);
        socket.emit('cars-reply', carDataBroker.cars());
    });

    socket.on('car-sub', (carId) => {
        logger.debug(`FEED: ${socket.handshake.address} sent 'car-sub ${carId}'`);
        carDataBroker.subscribe(socket, carId, function() {
            socket.emit('car-sub-ack', carId);
        });
    });

    socket.on('car-unsub', (carId) => {
        logger.debug(`FEED: ${socket.handshake.address} sent 'car-unsub ${carId}'`);
        carDataBroker.unsubscribe(socket, carId, function() {
            socket.emit('car-unsub-ack', carId);
        });
    });

    socket.on('disconnect', () => {
        logger.debug(`FEED: ${socket.handshake.address} disconnected`);
        carDataBroker.unsubscribeAll(socket, function() {
            socket.emit('disconnected', 'Unsubscribed from all cars');
        });
    });
});

io.of("/push").on('connection', function(socket) {
    logger.debug(`PUSH: new connection from ${socket.handshake.address}`);

    socket.on('sensor-update', (update) => {
        logger.debug(`PUSH: update received ${JSON.stringify(update)}`);
        carDataBroker.handleSensorUpdate(update);
    });
});

// ==== Frontend Routes =======================================================================================

// ---- Homepage ----------------------------------------------------------------------------------------------

app.get('/', (req, res, next) => {
    res.render('index');
});

// ==== Run Server ============================================================================================

server.listen(port, hostname, (err) => {
    if (err) {
        logger.error('Error on server start', err);
    } else {
        logger.info(`Server listening on ${hostname}:${port}`)
    }
});

/*
var positions = [ 'frontleft', 'frontright', 'rearleft', 'rearright' ];

// random updater
setInterval(function () {
    var cars = carDataBroker.cars();
    var car = cars[Math.floor(cars.length * Math.random())];
    var pos = positions[Math.floor(positions.length * Math.random())];
    var update = {
        carId: car,
        position: pos,
        ts: moment().format('YYYY-MM-DD HH:mm:ss'),
        pressure: Math.random(),
        temperature: Math.random(),
        speed: Math.random(),
        omega: Math.random()
    }
    //console.log(update);
    carDataBroker.handleSensorUpdate(update);
}, 1000);
*/
