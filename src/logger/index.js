'use strict'

const winston = require('winston')

module.exports = (log_level) => {
    var real_log_level = !log_level ? "info" : log_level

    var logger = new winston.Logger({
        transports: [
            new winston.transports.Console({
                level: real_log_level,
                handleExceptions: true,
                json: false,
                colorize: true
            })
        ],
        exitOnError: false
    })

    logger.stream = {
        write: function(message, encoding) {
            logger.info(message)
        }
    }

    return logger
}
