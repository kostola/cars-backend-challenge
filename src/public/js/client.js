// --- global vars

var subscriptions = {};
var socket = io.connect('http://localhost:3000/feed');

// --- global functions

function carBtnClicked(carId) {
    return function() {
        console.log(`${carId} clicked`)
        if (carId in subscriptions) {
            if (subscriptions[carId].subscribed) {
                socket.emit('car-unsub', carId);
                subscriptions[carId].subscribed = false;
            } else {
                socket.emit('car-sub', carId);
                subscriptions[carId].subscribed = true;
            }
            updateCarsStatus();
        } else {
            console.log(`Unknown car ${carId}`)
        }
    }
}

function updateCarsStatus() {
    console.log("Volvo" in subscriptions);
    console.log("alauela" in subscriptions);
    $('.car-div').remove();
    for (var carId in subscriptions) {
        var car = subscriptions[carId];
        if (car.subscribed) {
            $(`#${car.btnId}`).addClass('darken-2').removeClass('lighten-2');
            $('#cars-data').append(`<div class="col s12 m12 l12 car-div card-panel white" id="${car.divId}">
                <div class="car-title">${carId}</div>
                <div class="tyre-frontleft">
                    <div class="tyre-title">Front Left</div>
                    <div class="tyre-pressure">Pressure:</div>
                    <div class="tyre-temperature">Temperature:</div>
                    <div class="tyre-speed">Speed:</div>
                    <div class="tyre-omega">Omega:</div>
                </div>
                <div class="tyre-frontright">
                    <div class="tyre-title">Front Right</div>
                    <div class="tyre-pressure">Pressure:</div>
                    <div class="tyre-temperature">Temperature:</div>
                    <div class="tyre-speed">Speed:</div>
                    <div class="tyre-omega">Omega:</div>
                </div>
                <div class="tyre-rearleft">
                    <div class="tyre-title">Rear Left</div>
                    <div class="tyre-pressure">Pressure:</div>
                    <div class="tyre-temperature">Temperature:</div>
                    <div class="tyre-speed">Speed:</div>
                    <div class="tyre-omega">Omega:</div>
                </div>
                <div class="tyre-rearright">
                    <div class="tyre-title">Rear Right</div>
                    <div class="tyre-pressure">Pressure:</div>
                    <div class="tyre-temperature">Temperature:</div>
                    <div class="tyre-speed">Speed:</div>
                    <div class="tyre-omega">Omega:</div>
                </div>
            </div>`);
        } else {
            $(`#${car.btnId}`).addClass('lighten-2').removeClass('darken-2');
            $(`#${car.divId}`).remove();
        }
    }
}

function resetCars() {
    $('.car-btn').remove();
    $('.car-div').remove();
    subscriptions = {};
}

// --- materialize.css
$(document).ready( function() {
    // initializations
    $('.button-collapse').sideNav();
    $(".dropdown-button").dropdown();
    $('select').material_select();
});

// --- socket.io
socket.on('connect', function(data) {
    socket.emit('cars');
});

socket.on('disconnect', function() {
    console.log('disconnected asasas');
    resetCars();
})

socket.on('cars-reply', function(data) {
    console.log(data);
    subscriptions = {};
    for (var carIndex in data) {
        var carId    = data[carIndex];
        var carBtnId = `car-btn-${carIndex}`;
        var carDivId = `car-div-${carIndex}`;

        subscriptions[carId] = {
            index: carIndex,
            btnId: carBtnId,
            divId: carDivId,
            subscribed: false
        }

        $('#cars-menu').append(`<div class="col s12 m4 l4"><a class="btn indigo lighten-2 car-btn" id="${carBtnId}">${data[carIndex]}</a></div>`);
        $(`#${carBtnId}`).click(carBtnClicked(carId));
    }
    updateCarsStatus();
});

socket.on('cars-sub-ack', function(data) {
    console.log('cars-sub-ack: ' + data);
});

socket.on('cars-unsub-ack', function(data) {
    console.log('cars-unsub-ack: ' + data);
});

socket.on('car-update', function(update) {
    console.log('car-update: ' + JSON.stringify(update));
    var car = subscriptions[update.id];
    if (car.subscribed) {
        if (update.frontleft) {
            $(`#${car.divId} .tyre-frontleft .tyre-pressure`).text(`Pressure: ${update.frontleft.pressure}`);
            $(`#${car.divId} .tyre-frontleft .tyre-temperature`).text(`Temperature: ${update.frontleft.temperature}`);
            $(`#${car.divId} .tyre-frontleft .tyre-speed`).text(`Pressure: ${update.frontleft.speed}`);
            $(`#${car.divId} .tyre-frontleft .tyre-omega`).text(`Pressure: ${update.frontleft.omega}`);
        }
        if (update.frontright) {
            $(`#${car.divId} .tyre-frontright .tyre-pressure`).text(`Pressure: ${update.frontright.pressure}`);
            $(`#${car.divId} .tyre-frontright .tyre-temperature`).text(`Temperature: ${update.frontright.temperature}`);
            $(`#${car.divId} .tyre-frontright .tyre-speed`).text(`Pressure: ${update.frontright.speed}`);
            $(`#${car.divId} .tyre-frontright .tyre-omega`).text(`Pressure: ${update.frontright.omega}`);
        }
        if (update.rearleft) {
            $(`#${car.divId} .tyre-rearleft .tyre-pressure`).text(`Pressure: ${update.rearleft.pressure}`);
            $(`#${car.divId} .tyre-rearleft .tyre-temperature`).text(`Temperature: ${update.rearleft.temperature}`);
            $(`#${car.divId} .tyre-rearleft .tyre-speed`).text(`Pressure: ${update.rearleft.speed}`);
            $(`#${car.divId} .tyre-rearleft .tyre-omega`).text(`Pressure: ${update.rearleft.omega}`);
        }
        if (update.rearright) {
            $(`#${car.divId} .tyre-rearright .tyre-pressure`).text(`Pressure: ${update.rearright.pressure}`);
            $(`#${car.divId} .tyre-rearright .tyre-temperature`).text(`Temperature: ${update.rearright.temperature}`);
            $(`#${car.divId} .tyre-rearright .tyre-speed`).text(`Pressure: ${update.rearright.speed}`);
            $(`#${car.divId} .tyre-rearright .tyre-omega`).text(`Pressure: ${update.rearright.omega}`);
        }
    }
});
