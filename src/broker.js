'use strict'

var moment = require('moment');

var carsSubscribers = new Map();
var carsData = new Map();

exports.cars = function() {
    return Array.from(carsData.keys()).sort();
}

exports.handleSensorUpdate = function(update) {
    var carId = update.carId;
    this.initCar(carId);
    var pos   = update.position;
    console.log(pos);
    var tyre  = carsData.get(carId)[pos];
    var oldTs = !tyre.ts ? undefined : moment(tyre.ts);
    var newTs = moment(update.ts);
    if (!oldTs || oldTs.isBefore(newTs)) {
        carsData.get(carId)[pos].ts = update.ts;
        carsData.get(carId)[pos].pressure = update.pressure;
        carsData.get(carId)[pos].temperature = update.temperature;
        carsData.get(carId)[pos].speed = update.speed;
        carsData.get(carId)[pos].omega = !update.omega || update.omega < 0.0 ? undefined : update.omega;
        this.pushCarUpdates(carId);
    }
}

exports.initCar = function(carId) {
    if (! carsData.has(carId)) {
        carsData.set(carId, {
            id: carId,
            frontleft: {
                ts: undefined,
                pressure: undefined,
                temperature: undefined,
                speed: undefined,
                omega: undefined
            },
            frontright: {
                ts: undefined,
                pressure: undefined,
                temperature: undefined,
                speed: undefined,
                omega: undefined
            },
            rearleft: {
                ts: undefined,
                pressure: undefined,
                temperature: undefined,
                speed: undefined,
                omega: undefined
            },
            rearright: {
                ts: undefined,
                pressure: undefined,
                temperature: undefined,
                speed: undefined,
                omega: undefined
            }
        });
    }
}

exports.pushAllUpdates = function() {
    for (var carId in carsSubscribers) {
        this.pushCarUpdates(carId);
    }
}

exports.pushCarUpdates = function(carId) {
    if (carId in carsSubscribers && carsData.has(carId)) {
        var carSubs = carsSubscribers[carId];
        for (var i=0; i < carSubs.length; i++) {
            carSubs[i].volatile.emit('car-update', carsData.get(carId));
        }
    }
}

exports.setInitialCars = function(cars) {
    for(var i in cars) {
        this.initCar(cars[i]);
    }
}

exports.subscribe = function(socket, carId, callback) {
    if (typeof socket.cars === "undefined") {
        socket.cars = [];
    }
    if (socket.cars.indexOf(carId) == -1) {
        socket.cars.push(carId);
    }
    var carSubs = carsSubscribers[carId];
    if (typeof carSubs === "undefined") {
        carSubs = [];
        carsSubscribers[carId] = carSubs;
    }
    if (carSubs.indexOf(socket) == -1) {
        carSubs.push(socket);
    }
    callback();
}

exports.unsubscribe = function(socket, carId, callback) {
    if (typeof socket.cars === "undefined") {
        return;
    }
    var ndx = socket.cars.indexOf(carId);
    if (ndx !== -1) {
        socket.cars.splice(ndx, 1);
    }
    var carSubs = carsSubscribers[carId];
    if (typeof carSubs === "undefined") {
        return;
    }
    ndx = carSubs.indexOf(socket);
    if (ndx !== -1) {
        carSubs.splice(ndx, 1);
    }
    callback();
}

exports.unsubscribeAll = function(socket, callback) {
    if (typeof socket.cars === "undefined") {
        return;
    }
    for (var i=0; i < socket.cars.length; i++) {
        this.unsubscribe(socket, socket.cars[i], function() {});
    }
    callback();
}
