'use strict';

const readline = require('readline');
const fs = require('fs');
const CSV = require('csv-string');
const ArgumentParser = require('argparse').ArgumentParser;
const moment = require('moment');

const parser = new ArgumentParser({
  version: '1.0.0',
  addHelp: true,
  description: 'Simple tool that pushes fake sensors data from a CSV file'
});

parser.addArgument([ '-f', '--file' ], {
    help: 'File that will be parsed',
    metavar: 'FILE',
    dest: 'file',
    type: 'string',
    defaultValue: 'data/cars.csv'
});
parser.addArgument([ '-u', '--url' ], {
    help: 'Destination URL',
    metavar: 'URL',
    dest: 'url',
    type: 'string',
    defaultValue: 'http://localhost:3000/push'
});
parser.addArgument( [ '-a', '--accel' ], {
    help: 'Acceleration',
    metavar: 'ACCEL',
    dest: 'accel',
    type: 'int',
    defaultValue: 60
});

const args = parser.parseArgs();
if (args.accel <= 0) {
    args.accel = 60;
}

console.log(`Trying to send data from "${args.file}" to push stream at "${args.url}"`);

const socket = require('socket.io-client')(args.url);
var lastTs = undefined;

socket.on('connect', function(){
    console.log("Connected to push stream");

    const rl = readline.createInterface({
        input: fs.createReadStream(args.file)
    });

    rl.on('line', function (line) {
        const csvlines = CSV.parse(line);
        if (csvlines.length == 1) {
            const values = csvlines[0];
            if (values.length == 8 && values[7] !== 'Car_id') {
                var newTs = moment(values[1]);

                if (!!lastTs) {
                    var delta = newTs - lastTs;
                    if (delta > 0) {
                        // very ugly hack to overcome absence of synchronous sleep function in Node.js
                        var waitTill = moment().add(delta / args.accel, 'ms');
                        while (waitTill.isAfter(moment())) {}
                    }
                }

                var update = {
                    carId: values[7],
                    position: values[3].toLowerCase().replace(" ", "").replace("rigth", "right"),
                    ts: values[1],
                    pressure: values[2],
                    temperature: values[4],
                    speed: values[6],
                    omega: values[5]
                }

                console.log("SENT: " + JSON.stringify(update));
                socket.emit('sensor-update', update);
                lastTs = newTs;
            }
        }
    });
});
