
# cars-backend-challenge

This repo contains my solution to the technical challenge for backend developer.

# Technologies

The two core technologies I used to develop this project are:

* **[Node.js](https://nodejs.org/):** I used this techonology because it allows to quickly develop a backend application easily handling concurrency thanks to its event-loop based structure.

* **[Socket.io](https://socket.io/):** I used this technology to stream data from sensors to the backend and from the backend to the end-users (which, in this demo, is a web frontend).

# How to run backend

1. Checkout this repo and make sure that `node` (>= 8) and `npm` are installed in your machine
2. Run `npm install` from the root of the repo to install dependencies.
3. Run `npm start` to start the server.

# Web frontend

By default, the server starts `localhost:3000`. To access the web frontend, open a browser and go to `http://localhost:3000`.

## How to use the web frontend

After opening the page, the web frontend should show you some buttons, one per each car you can subscribe to, right under the (empty) navigation bar.

To subscribe to updates for a specific car, click to the button with its name. To show that you're subscribed, it will become **slightly darker**. You should see a corresponding box appearing under the buttons area.

# Pusher tool

The pusher tool allows you to push data to the backend, simulating the real sensors. It takes the values from a csv file with the same format of the one you gave me.

To run it, launch `npm run pusher` from the root of the repo.

## What it does

1. It tries to connect to the websocket.
2. It streams the data that it reads from the csv file, line by line, trying to maintain the same interval between the timestamps of the lines.

## Parameters

Pusher tool accepts some parameters, that you can send with `npm run pusher -- <params>`.

The `-h` or `--help` parameter will print this help:

```
usage: pusher.js [-h] [-v] [-f FILE] [-u URL] [-a ACCEL]

Simple tool that pushes fake sensors data from a CSV file

Optional arguments:
  -h, --help               Show this help message and exit.
  -v, --version            Show program's version number and exit.
  -f FILE, --file FILE     File that will be parsed
  -u URL, --url URL        Destination URL
  -a ACCEL, --accel ACCEL  Acceleration
```

I think that `file` and `url` params are pretty straightforward. The `accel` parameter specifies the acceleration factor when simulating intervals between different updates (read from the CSV), by default it's `60`, which means that the tool will wait 1 second for each minute of delay between timestamps.

# How to try the demo

1. Install the dependencies with `npm install`
2. Run backend with `npm start`
3. Open web frontend at `http://localhost:3000`
4. Subscribe to at least one car
5. Run pusher tool to see values moving

**WARNING: if you run pusher tool from the same host as the backend, sometimes messages won't immediately arrive to the backend because the OS may buffer data. Socket.io unfortunately doesn't allow flushing data, so please keep waiting.**
